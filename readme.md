# readme.md - README for MvcForms.Core.GuiCs v0.8

## Desktop TUI app demo, on Linux, in C# / DotNet[8 / Terminal.Gui (gui.cs), using VSCode

![MvcForms.Core.GuiCs.png](./MvcForms.Core.GuiCs.png?raw=true "Screenshot")

### Purpose

To illustrate a working Desktop TUI app in c# on .Net (Core). Uses Terminal.Gui where WinForms was being used.

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.cs to remove the ViewModel.FileOpen() call.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Instructions/example of adding Terminal.GUI to dotnet and creating app ui in VSCode

dotnet add package Terminal.Gui --version 1.16.0

### Install package for app configuration

~in folder for project use:
dotnet add package System.Configuration.ConfigurationManager

### Terminal.Gui source

<https://github.com/migueldeicaza/gui.cs>

### Terminal.Gui Overview

<https://migueldeicaza.github.io/gui.cs/articles/overview.html>

### Example of creating an app

<https://markjames.dev/2020-10-25-developing-a-cli-music-player-csharp/>

### Example of creating app

<https://curatedcsharp.com/p/gui-migueldeicaza-guics/index.html>

## Terminal Gui Designer (Alpha)

### Install

dotnet tool install --global TerminalGuiDesigner

### UnInstall

dotnet tool uninstall --global TerminalGuiDesigner

### Update

dotnet tool update --global TerminalGuiDesigner

### Run

TerminalGuiDesigner Form1.cs

### Color Issues? Try

TerminalGuiDesigner --usc

## Issues

~error output to STDERR is showing up on screen during run, unless you redirect to a log: dotnet run 2>log.out

~setting explicit window size seems to be ignored

~on GhostBSD, builds OK, but gets runtime error "Curses failed to initialize, the exception is: Error loading native library "libncursesw.so.6, libncursesw.so.5", even after ncurses library 6.4_1 installed. If so, (Thanks @tznind) put 
 ```
 Application.UseSystemConsole = true;
 ```
  before 
 ```
  Application.Init();
```

## Enhancements

0.15:
~Provide work-around to ncurses lib error on GhostBSD (FreeBSD).

0.14:
~Move Application.Shutdown from finally to try.

0.13:
~upgrade Terminal.Gui from 1.16.0 to 1.17.0 via NuGet

0.12:
~upgrade Terminal.Gui from 1.15.1 to 1.16.0 via NuGet

0.11:
~fixed missing right/bottom borders; setting Width/Height from  global::MvcForms.Core.AvaloniaUI.Properties.Settings.Default.Location.X/Y in BindSizeAndLocation does not play well with Terminal.Gui.

0.10:
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.
~change field widths and other dimensions from Dim.Percent(n) to n
~upgrade Terminal.Gui from 1.14.x to 1.15.1 via NuGet

0.9:
~Update to .net8.0
~Update Terminal.Gui to v1.14.1
~Fix startup handling of settings filename in App.config.

0.8:
~move some progress/stat calls into VM
~fix quit trapping

0.7:
~Set new Website property in AssemblyInfo (new property defined in AssemblyInfoBase).

0.6:
~Changes to client app resulting from refactoring of Ssepan.Application.Core[.*].

## Contact

Steve Sepan
<sjsepan@yahoo.com>
6/5/2024
