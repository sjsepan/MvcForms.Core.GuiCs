using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.GuiCs;
using MvcLibrary.Core;
using Terminal.Gui;
//TODO:v2.0: see https://github.com/gui-cs/Terminal.Gui/discussions/2448

namespace MvcForms.Core.GuiCs
{
    /// <summary>
    /// This is the View.
    /// Can base window on Window or its subclasses
    /// </summary>
    public class MvcView :
        Window,
        INotifyPropertyChanged
    {
        #region Declarations
        //TODO:need to simulate marquee of ProgressBar control
        private const string StatusBar_MarqueeOn = "   ***    ";
        private const string StatusBar_MarqueeOff = "          ";
        // Use this enum to access status bar items via index, as in:
        //  Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Status].Title
        // or access them directly , as in:
        //  _statusMessage.Title
        private enum StatusBar_ItemIndex
        {
            Status = 0,
            Error = 1,
            Progress = 2,
            Action = 3,
            Dirty = 4
        }

        #region declare menu
        private MenuBar menu;
        #endregion declare menu

        #region declare status
        private StatusBar statusBar;
        private StatusItem _statusMessage;
        private StatusItem _errorMessage;
        private StatusItem _progressBar;
        private StatusItem _pictureAction;
        private StatusItem _pictureDirty;
        #endregion declare status

        #region declare controls
        private Label _lblSomeInt;
        private Label _lblSomeOtherInt;
        private Label _lblStillAnotherInt;
        private TextField _txtSomeInt;
        private TextField _txtSomeOtherInt;
        private TextField _txtStillAnotherInt;
        private Label _lblSomeString;
        private Label _lblSomeOtherString;
        private Label _lblStillAnotherString;
        private TextField _txtSomeString;
        private TextField _txtSomeOtherString;
        private TextField _txtStillAnotherString;
        private Label _lblSomeBoolean;
        private Label _lblSomeOtherBoolean;
        private Label _lblStillAnotherBoolean;
        private CheckBox _chkSomeBoolean;
        private CheckBox _chkSomeOtherBoolean;
        private CheckBox _chkStillAnotherBoolean;
        private Label _lblSomeDate;
        private Label _lblSomeOtherDate;
        private Label _lblStillAnotherDate;
        private DateField _dtSomeDate;
        private DateField _dtSomeOtherDate;
        private DateField _dtStillAnotherDate;
        private Button cmdRun;
		#endregion declare controls

        protected MVCViewModel ViewModel;
        #endregion Declarations

        #region Constructors
        // By using Dim.Fill(), it will automatically resize without manual intervention
        public MvcView(string title) :
           base(title)
        {
            try
            {
                Closing += Window_Closing;
                KeyPress += Window_KeyPress;

                InitStyle();
                InitControls();

                //use direct calls until ViewModel ready
                SetStatusMessage("");
                SetErrorMessage("");

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public void InitStyle()
        {
            // LayoutStyle = LayoutStyle.Computed;
            X = 0;
            ///Y=*1*/, // Leave one row for the toplevel menu
            //Note:Y does not have to be 1 to leave space if menu attached to current view; 1 causes extra space
            Y = 0;
            Width = Dim.Fill();
            Height = Dim.Fill();
        }

        private void InitControls()
        {
			#region define menu
			menu = new MenuBar(
			[
                new MenuBarItem("_File",
                    new MenuItem[]
                    {
                        new("_New", "", FileNew_Clicked, null, null, Key.CtrlMask|Key.N),
                        new("_Open", "",FileOpen_Clicked, null, null, Key.CtrlMask|Key.O),
                        new("_Save", "",FileSave_Clicked, null, null, Key.CtrlMask|Key.S),
                        new("Save _As", "",FileSaveAs_Clicked, null, null, Key.Null),
                        new("---", "", null, null, null, Key.Null),
                        new("_Print", "",FilePrint_Clicked, null, null, Key.CtrlMask|Key.P),
                        new("P_rint Preview", "",FilePrintPreview_Clicked, null, null, Key.Null),
                        new("---", "", null, null, null, Key.Null),
                        new("_Quit", "", FileQuit_Clicked, null, null, Key.Null)//Key.CtrlMask|Key.Q
                    }
                ), // end of file menu
                new MenuBarItem("_Edit",
                    new MenuItem[]
                    {
                        new("_Undo", "", EditUndo_Clicked, null, null, Key.CtrlMask|Key.Z),
                        new("_Redo", "", EditRedo_Clicked, null, null, Key.CtrlMask|Key.Y),
                        new("---", "", null, null, null, Key.Null),
                        new("Select _All", "", EditSelectAll_Clicked, null, null, Key.CtrlMask|Key.A),
                        new("Cu_t", "", EditCut_Clicked, null, null, Key.CtrlMask|Key.X),
                        new("C_opy", "", EditCopy_Clicked, null, null, Key.CtrlMask|Key.C),
                        new("_Paste", "", EditPaste_Clicked, null, null, Key.CtrlMask|Key.V),
                        new("Paste _Special...", "", EditPasteSpecial_Clicked, null, null, Key.Null),
                        new("_Delete", "", EditDelete_Clicked, null, null, Key.Delete),
                        new("---", "", null, null, null, Key.Null),
                        new("F_ind", "", EditFind_Clicked, null, null, Key.CtrlMask|Key.F),
                        new("R_eplace...", "", EditReplace_Clicked, null, null, Key.CtrlMask|Key.H),
                        new("---", "", null, null, null, Key.Null),
                        new("Re_fresh", "", EditRefresh_Clicked, null, null, Key.F5),
                        new("---", "", null, null, null, Key.Null),
                        new("Prefere_nces...", "", EditPreferences_Clicked, null, null, Key.Null)
                    }
                ), // end of edit menu
                new MenuBarItem("_Window",
                    new MenuItem[]
                    {
                        new("_New Window", "", WindowNewWindow_Clicked, null, null, Key.Null),
                        new("_Tile", "", WindowTile_Clicked, null, null, Key.Null),
                        new("_Cascade", "", WindowCascade_Clicked, null, null, Key.Null),
                        new("_Arrange All", "", WindowArrangeAll_Clicked, null, null, Key.Null),
                        new("---", "", null, null, null, Key.Null),
                        new("_Hide", "", WindowHide_Clicked, null, null, Key.Null),
                        new("_Show", "", WindowShow_Clicked, null, null, Key.Null)
                    }
                ), // end of window menu
                new MenuBarItem("_Help",
                    new MenuItem[]
					{
						new("_Contents", "", HelpContents_Clicked, null, null, Key.F1),
						new("_Index", "", HelpIndex_Clicked, null, null, Key.Null),
						new("_Online Help", "", HelpOnlineHelp_Clicked, null, null, Key.Null),
						new("---", "", null, null, null, Key.Null),
						new("_License Information", "", HelpLicenseInformation_Clicked, null, null, Key.Null),
						new("_Check for Updates", "", HelpCheckForUpdates_Clicked, null, null, Key.Null),
						new("---", "", null, null, null, Key.Null),
						new("_About", "", HelpAbout_Clicked, null, null, Key.Null)
					}
				) // end of the help menu
            ]);
			#endregion define menu

			#region define status
			statusBar = new StatusBar(
			items: [
				_statusMessage = new StatusItem(Key.Null, "", null),
				_errorMessage = new StatusItem(Key.Null, "", null),
				_progressBar = new StatusItem(Key.Null, StatusBar_MarqueeOff, null),
				_pictureAction = new StatusItem(Key.Null, "", null),
				_pictureDirty = new StatusItem(Key.Null, "", null)
			])
			{
				//Visible = true
			};
			//Examples
			// statusBar.Items[(int)StatusBar_ItemIndex.Status].Title = "NewStatusMessage";
			// or
			//_statusMessage.Title = "NewStatusMessage";
			// statusBar.Items[(int)StatusBar_ItemIndex.Error].Title = "NewErrorMessage";
			// or
			//_errorMessage.Title = "NewErrorMessage";
			// statusBar.Items[(int)StatusBar_ItemIndex.Progress].Title = "   ***    ";
			// or
			//_progressBar.Title = "   ***    ";
			// statusBar.Items[(int)StatusBar_ItemIndex.Action].Title = "New";
			// or
			//_pictureAction.Title = "New";
			// statusBar.Items[(int)StatusBar_ItemIndex.Dirty].Title = "";
			// or
			//_pictureDirty.Title = "";
			#endregion define status

			#region define controls
			_lblSomeInt = new Label("Some Integer:")
            {
                X = 0,
                Y = 0,
                Width = 20
            };

            _lblSomeOtherInt = new Label("Other Integer:")
            {
                X = 25,
                Y = Pos.Top(_lblSomeInt),
                Width = 20
            };

            _lblStillAnotherInt = new Label("Another Integer:")
            {
                X = 50,
                Y = Pos.Top(_lblSomeInt),
                Width = 20
            };

            _txtSomeInt = new TextField("")
            {
                X = Pos.Left(_lblSomeInt),
                Y = Pos.Top(_lblSomeInt) + 1,
                Width = 20
            };

            _txtSomeOtherInt = new TextField("")
            {
                X = 25,
                Y = Pos.Top(_lblSomeOtherInt) + 1,
                Width = 20
            };

            _txtStillAnotherInt = new TextField("")
            {
                X = 50,
                Y = Pos.Top(_lblStillAnotherInt) + 1,
                Width = 20
            };

            _lblSomeString = new Label("Some string:")
            {
                X = 0,
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = 20
            };

            _lblSomeOtherString = new Label("Other string:")
            {
                X = 25,
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = 20
            };

            _lblStillAnotherString = new Label("Another string:")
            {
                X = 50,
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = 20
            };

            _txtSomeString = new TextField("")
            {
                X = Pos.Left(_lblSomeString),
                Y = Pos.Top(_lblSomeString) + 1,
                Width = 20
            };

            _txtSomeOtherString = new TextField("")
            {
                X = 25,
                Y = Pos.Top(_lblSomeOtherString) + 1,
                Width = 20
            };

            _txtStillAnotherString = new TextField("")
            {
                X = 50,
                Y = Pos.Top(_lblStillAnotherString) + 1,
                Width = 20
            };

            _lblSomeBoolean = new Label("Some bool:")
            {
                X = 0,
                Y = Pos.Top(_txtSomeString) + 1,
                Width = 20
            };

            _lblSomeOtherBoolean = new Label("Other bool:")
            {
                X = 25,
                Y = Pos.Top(_lblSomeBoolean),
                Width = 20
            };

            _lblStillAnotherBoolean = new Label("Another bool:")
            {
                X = 50,
                Y = Pos.Top(_lblSomeBoolean),
                Width = 20
            };

            _chkSomeBoolean = new CheckBox("")
            {
                X = Pos.Left(_lblSomeBoolean),
                Y = Pos.Top(_lblSomeBoolean) + 1,
                Width = 20
            };

            _chkSomeOtherBoolean = new CheckBox("")
            {
                X = 25,
                Y = Pos.Top(_lblSomeOtherBoolean) + 1,
                Width = 20
            };

            _chkStillAnotherBoolean = new CheckBox("")
            {
                X = 50,
                Y = Pos.Top(_lblStillAnotherBoolean) + 1,
                Width = 20
            };

            _lblSomeDate = new Label("Some Date:")
            {
                X = 0,
                Y = Pos.Top(_chkSomeBoolean) + 1,
                Width = 20
            };

            _lblSomeOtherDate = new Label("Other Date:")
            {
                X = 25,
                Y = Pos.Top(_lblSomeDate),
                Width = 20
            };

            _lblStillAnotherDate = new Label("Another Date:")
            {
                X = 50,
                Y = Pos.Top(_lblSomeDate),
                Width = 20
            };

            _dtSomeDate = new DateField(DateTime.Now)
            {
                X = Pos.Left(_lblSomeDate),
                Y = Pos.Top(_lblSomeDate) + 1,
                Width = 20
            };

            _dtSomeOtherDate = new DateField(DateTime.Now)
            {
                X = 25,
                Y = Pos.Top(_lblSomeOtherDate) + 1,
                Width = 20
            };

            _dtStillAnotherDate = new DateField(DateTime.Now)
            {
                X = 50,
                Y = Pos.Top(_lblStillAnotherDate) + 1,
                Width = 20
            };

            cmdRun = new Button("Run")
            {
                X = 75,
                Y = 1
            };
            cmdRun.Clicked += CmdRun_Clicked;
            #endregion define controls

            #region add controls to top
            // Application.Top.MenuBar = menu;//any benefit?
            Application.Top.Add(menu);
            //Application.Top.StatusBar = statusBar;
            Application.Top.Add(statusBar);
            #endregion add controls to top

            #region add controls to container
            //Note:order is significant if a given control is positioned relative to another
            Add(_lblSomeInt);
            Add(_lblSomeOtherInt);
            Add(_lblStillAnotherInt);
            Add(_txtSomeInt);
            Add(_txtSomeOtherInt);
            Add(_txtStillAnotherInt);
            Add(_lblSomeString);
            Add(_lblSomeOtherString);
            Add(_lblStillAnotherString);
            Add(_txtSomeString);
            Add(_txtSomeOtherString);
            Add(_txtStillAnotherString);
            Add(_lblSomeBoolean);
            Add(_lblSomeOtherBoolean);
            Add(_lblStillAnotherBoolean);
            Add(_chkSomeBoolean);
            Add(_chkSomeOtherBoolean);
            Add(_chkStillAnotherBoolean);
            Add(_lblSomeDate);
            Add(_lblSomeOtherDate);
            Add(_lblStillAnotherDate);
            Add(_dtSomeDate);
            Add(_dtSomeOtherDate);
            Add(_dtStillAnotherDate);

            Add(cmdRun);
            #endregion add controls to container

            #region bind control events
            //add form field handlers
            _txtSomeInt.TextChanged += SomeInt_TextChanged;
            _txtSomeOtherInt.TextChanged += SomeOtherInt_TextChanged;
            _txtStillAnotherInt.TextChanged += StillAnotherInt_TextChanged;
            _txtSomeString.TextChanged += SomeString_TextChanged;
            _txtSomeOtherString.TextChanged += SomeOtherString_TextChanged;
            _txtStillAnotherString.TextChanged += StillAnotherString_TextChanged;
            _chkSomeBoolean.Toggled += ChkSomeBoolean_Toggled;
            _chkSomeOtherBoolean.Toggled += ChkSomeOtherBoolean_Toggled;
            _chkStillAnotherBoolean.Toggled += ChkStillAnotherBoolean_Toggled;
            _dtSomeDate.DateChanged += DtSomeDate_DateChanged;
            _dtSomeOtherDate.DateChanged += DtSomeOtherDate_DateChanged;
            _dtStillAnotherDate.DateChanged += DtStillAnotherDate_DateChanged;

            #endregion bind control events

            #region additional setup
            // login window will be appear on the center screen
            // ChildView child = new ChildView(this);
            // Add(child);
            #endregion additional setup

        }
        #endregion Constructors

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        #endregion Properties

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Handlers

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    //_pictureAction.Visible = (ViewModel.ActionIconIsVisible);
                    if (ViewModel?.DirtyIconIsVisible == true)
                    {
                        StartActionIcon(ViewModel != null ? (ViewModel.ActionIconImage ?? "") : "");
                    }
                    else
                    {
                        StopActionIcon();
                    }
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    //Note:use ActionIconIsVisible
                    // _pictureAction.IconName = (ViewModel != null ? ViewModel.ActionIconImage : (string)null);
                    // StartActionIcon((ViewModel != null ? (ViewModel.ActionIconImage != null ? ViewModel.ActionIconImage : "") : ""));
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    //_pictureDirty.Visible = (ViewModel.DirtyIconIsVisible);
                    SetDirtyIcon(ViewModel?.DirtyIconIsVisible == true);
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    //Note:use DirtyIconIsVisible
                    // _pictureDirty.Title = ((ViewModel != null ? ViewModel.DirtyIconImage : (string)null));
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    // _statusMessage.Title = (ViewModel != null ? ViewModel.StatusMessage : (string)null);
                    SetStatusMessage(ViewModel != null ? (ViewModel.StatusMessage ?? "") : "");
					//Console.WriteLine(ViewModel != null ? (ViewModel.StatusMessage ?? "") : "");
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
				}
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    // _errorMessage.Title = (ViewModel != null ? ViewModel.ErrorMessage : (string)null);
                    SetErrorMessage(ViewModel != null ? (ViewModel.ErrorMessage ?? "") : "");
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    //TODO:implement tooltip on errormessage
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    //NOTE:use ProgressBarIsVisible
                    //todo implement percentage
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    //NOTE:use ProgressBarIsVisible
                    //TODO:implement maximum
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    //NOTE:use ProgressBarIsVisible
                    //TODO:implement minimum
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    //NOTE:use ProgressBarIsVisible
                    //TODO:implement step
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    //NOTE:use ProgressBarIsVisible
                    //TODO:implement marquee vs percentage
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    if (ViewModel.ProgressBarIsVisible)
                    {
                        StartProgressBar();
                    }
                    else
                    {
                        StopProgressBar();
                    }
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                    _txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                    _chkSomeBoolean.Checked = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                    _txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                    _txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                    _chkStillAnotherBoolean.Checked = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                    _txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                    _txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                    _chkSomeOtherBoolean.Checked = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                    _txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
#if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
#endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
#if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
#endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form Events
        //TODO:2.0:revisit and use object sender, EventArgs args signature

        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Clicked
        /// </summary>
        private void Window_Closing<TopLevelClosingEventArgs>(/*object sender,*/TopLevelClosingEventArgs e)
        {
            bool resultDontQuit = false;

            try
            {
                ViewModel.FileExit(ref resultDontQuit);
                //e.RetVal = resultDontQuit;//NOTE:no return value in this 'e'.
                if (!resultDontQuit)
                {
                    ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                    DisposeSettings();
                    ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                    ViewModel = null;

                    Application.Top.RequestStop();
                }
                else
                {
                    //Quit cancelled
                }
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = "";

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// TODO:handle ProcessCmdKey
        /// </summary>
        // [ GLib.ConnectBefore ] // need this to allow program to intercept the key first.
        private static void Window_KeyPress<KeyEventEventArgs>(/*object sender,*/ KeyEventEventArgs e)
        {
            //     bool returnValue = default(bool);
            try
            {

                // Implement the Escape / Cancel keystroke
                // if (keyData == Keys.Cancel || keyData == Keys.Escape)
                // {
                //     //if a long-running cancellable-action has registered
                //     //an escapable-event, trigger it
                //     InvokeActionCancel();

                //     // This keystroke was handled,
                //     //don't pass to the control with the focus
                //     returnValue = true;
                // }
                // else
                // {
                //     returnValue = base.ProcessCmdKey(ref msg, keyData);
                // }

            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            //     return returnValue;
        }

        #endregion Form Events

        #region Control Events
        // private void cmdRun_Click(object sender, EventArgs e)
        // {
        //     ViewModel.DoSomething();
        // }

        // private void ButtonColor_Clicked(object sender, EventArgs e)
        // {
        //     ViewModel.GetColor();
        // }

        // private void ButtonFont_Clicked(object sender, EventArgs e)
        // {
        //     ViewModel.GetFont();
        // }

        //TODO:v2.0:revisit and remove nstack ustring
        private void SomeInt_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
			try
			{
				// Console.WriteLine("SomeInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.SomeInt =
						int.TryParse(_txtSomeInt.Text.ToString(), out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        //TODO:v2.0:revisit and remove nstack ustring
        private void SomeOtherInt_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
			try
			{
				// Console.WriteLine("SomeOtherInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt =
						int.TryParse(_txtSomeOtherInt.Text.ToString(), out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        //TODO:v2.0:revisit and remove nstack ustring
        private void StillAnotherInt_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
			try
			{
				// Console.WriteLine("StillAnotherInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt =
						int.TryParse(_txtStillAnotherInt.Text.ToString(), out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        //TODO:v2.0:revisit and remove nstack ustring
        private void SomeString_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
            ModelController<MVCModel>.Model.SomeString = _txtSomeString.Text.ToString();
        }

        //TODO:v2.0:revisit and remove nstack ustring
        private void SomeOtherString_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = _txtSomeOtherString.Text.ToString();
        }

        //TODO:v2.0:revisit and remove nstack ustring
        private void StillAnotherString_TextChanged(/*object sender,*/ /*EventArgs e*/ NStack.ustring s)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = _txtStillAnotherString.Text.ToString();
        }

        private void ChkSomeBoolean_Toggled(/*object sender,*/ /*EventArgs e*/ bool b)
        {
            ModelController<MVCModel>.Model.SomeBoolean = _chkSomeBoolean.Checked;
        }

        private void ChkSomeOtherBoolean_Toggled(/*object sender,*/ /*EventArgs e*/ bool b)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = _chkSomeOtherBoolean.Checked;
        }

        private void ChkStillAnotherBoolean_Toggled(/*object sender,*/ /*EventArgs e*/ bool b)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = _chkStillAnotherBoolean.Checked;
        }

        private void DtSomeDate_DateChanged(/*object sender,*/ /*EventArgs e*/ DateTimeEventArgs<DateTime> d)
        {
            ModelController<MVCModel>.Model.SomeBoolean = _chkSomeBoolean.Checked;
        }

        private void DtSomeOtherDate_DateChanged(/*object sender,*/ /*EventArgs e*/ DateTimeEventArgs<DateTime> d)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = _chkSomeOtherBoolean.Checked;
        }

        private void DtStillAnotherDate_DateChanged(/*object sender,*/ /*EventArgs e*/ DateTimeEventArgs<DateTime> d)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = _chkStillAnotherBoolean.Checked;
        }

        private void CmdRun_Clicked()
        {
            ViewModel.DoSomething();
            //do something else
            RunAction();
        }
        #endregion Control Events

        #region Menu Events

        private void FileNew_Clicked()
        {
            ViewModel.FileNew();
        }

        private void FileOpen_Clicked()
        {
            ViewModel.FileOpen();
        }

        private void FileSave_Clicked()
        {
            ViewModel.FileSave();
        }

        private void FileSaveAs_Clicked()
        {
            ViewModel.FileSaveAs();
        }

        private void FilePrint_Clicked()
        {
            ViewModel.FilePrint();
        }

        private void FilePrintPreview_Clicked()
        {
            ViewModel.FilePrintPreview();
        }

        private void FileQuit_Clicked()
        {
			// FileQuitAction();
			//this.RequestStop();
			Window_Closing(new ToplevelClosingEventArgs(this));
        }

        private void EditUndo_Clicked()
        {
            ViewModel.EditUndo();
        }
        private void EditRedo_Clicked()
        {
            ViewModel.EditRedo();
        }

        private void EditSelectAll_Clicked()
        {
            ViewModel.EditSelectAll();
        }

        private void EditCut_Clicked()
        {
            ViewModel.EditCut();
        }

        private void EditCopy_Clicked()
        {
            ViewModel.EditCopy();
        }

        private void EditPaste_Clicked()
        {
            ViewModel.EditPaste();
        }

        private void EditPasteSpecial_Clicked()
        {
            ViewModel.EditPasteSpecial();
        }

        private void EditDelete_Clicked()
        {
            ViewModel.EditDelete();
        }

        private void EditFind_Clicked()
        {
            ViewModel.EditFind();
        }

        private void EditReplace_Clicked()
        {
            ViewModel.EditReplace();
        }

        private void EditRefresh_Clicked()
        {
            ViewModel.EditRefresh();
        }

        private void EditPreferences_Clicked()
        {
            ViewModel.EditPreferences();
        }

        private void WindowNewWindow_Clicked()
        {
            ViewModel.WindowNewWindow();
        }
        private void WindowTile_Clicked()
        {
            ViewModel.WindowTile();
        }
        private void WindowCascade_Clicked()
        {
            ViewModel.WindowCascade();
        }
        private void WindowArrangeAll_Clicked()
        {
            ViewModel.WindowArrangeAll();
        }
        private void WindowHide_Clicked()
        {
            ViewModel.WindowHide();
        }
        private void WindowShow_Clicked()
        {
            ViewModel.WindowShow();
        }
        private void HelpContents_Clicked()
        {
            ViewModel.HelpContents();
        }
        private void HelpIndex_Clicked()
        {
            ViewModel.HelpIndex();
        }
        private void HelpOnlineHelp_Clicked()
        {
            ViewModel.HelpOnlineHelp();
        }
        private void HelpLicenseInformation_Clicked()
        {
            ViewModel.HelpLicenceInformation();
        }
        private void HelpCheckForUpdates_Clicked()
        {
            ViewModel.HelpCheckForUpdates();
        }
        private void HelpAbout_Clicked()
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu Events

        #endregion Handlers

        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            FileDialogInfo<Window, string> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();

				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<Window, string>
					(
						parent: this,
						modal: true,
						title: null,
						response: null,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters: [
							"MvcSettings files (*.mvcsettings)|*.mvcsettings",
							"JSON files (*.json)|*.json",
							"XML files (*.xml)|*.xml",
							"All files (*.*)|*.*"
						],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
					)
					{
						//set dialog caption
						Title = Title.ToString()
					};

				//class to handle standard behaviors
				ViewModelController<string, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, string>()
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "New", "New" },
                            { "Open", "Open" },
                            { "Save", "Save" },
                            { "SaveAs", "SaveAs" },
                            { "Print", "Print" },
                            { "PrintPreview", "PrintPreview" },
                            { "Quit", "Quit" },
                            { "Undo", "Undo" },
                            { "Redo", "Redo" },
                            { "SelectAll", "SelectAll" },
                            { "Cut", "Cut" },
                            { "Copy", "Copy" },
                            { "Paste", "Paste" },
                            { "PasteSpecial", "PasteSpecial" },
                            { "Delete", "Delete" },
                            { "Find", "Find" },
                            { "Replace", "FindAndReplace" },
                            { "Refresh", "Refresh" },
                            { "Preferences", "Preferences" },
                            { "Properties", "Properties" },
                            { "NewWindow", "NewWindow" },
                            { "Tile", "Tile" },
                            { "Cascade", "Cascade" },
                            { "ArrangeAll", "ArrangeAll" },
                            { "Hide", "Hide" },
                            { "Show", "Show" },
                            { "Contents", "Help" },
                            { "Index", "Index" },
                            { "OnlineHelp", "OnlineHelp" },
                            { "LicenceInformation", "LicenceInformation" },
                            { "CheckForUpdates", "CheckForUpdates" },
                            { "About", "About" }
                        },
                        settingsFileDialogInfo
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<string, MVCViewModel>.ViewModel[ViewName];

				// BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo<Window, string, object, string, NStack.ustring[]> questionMessageDialogInfo = null;
            string errorMessage = null;

            //save user and application settings
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo<Window, string, object, string, NStack.ustring[]>
                (
                    this,
                    true,
                    Title.ToString(),
                    null,
                    "Question",
                    [MVCViewModel.Button_Yes, MVCViewModel.Button_No],
                    "Save changes?",
                    null
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
					case MVCViewModel.Button_Yes:
						//SAVE
						ViewModel.FileSave();

						break;

					case MVCViewModel.Button_No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        // protected void _Run()
        // {
        //     //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        // }
        #endregion FormAppBase

        #region Actions
        // private void DoSomething()
        // {
        //     for (int i = 0; i < 3; i++)
        //     {
        //         //_progressBar.Pulse();
        //         Application.DoEvents();
        //         Task.Delay(1000);
        //     }
        // }

        private void RunAction()
        {
            _chkStillAnotherBoolean.Checked = _chkSomeOtherBoolean.Checked;
            _chkSomeOtherBoolean.Checked = _chkSomeBoolean.Checked;
            _chkSomeBoolean.Checked = !_chkSomeBoolean.Checked;
        }
        // private void FileQuitAction()
        // {
        //     // bool dontQuit = false;
        //     // //Application.RequestStop();
        //     // SetStatusMessage("Quit" + ACTION_IN_PROGRESS);
        //     // StartProgressBar();
        //     // ViewModel.FileExit(ref dontQuit);//handles UI, returns user decision
        //     // StopProgressBar();
        //     // SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        //     bool resultDontQuit = false;

        //     try
        //     {
        //         ViewModel.StartProgressBar(_menuFileQuit.Help.ToString() + FormsViewModel<string, MVCSettings, MVCModel, MvcView>.ACTION_IN_PROGRESS, "", null, true, 33);
        //         // StartActionIcon(Gtk.Stock.Undo); //_menuEditUndo.Image.?

        //         ViewModel.FileExit(ref resultDontQuit);
        //         if (!resultDontQuit)
        //         {
        //             ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
        //             DisposeSettings();
        //             ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

        //             ViewModel = null;

        //             Application.Top.RequestStop();
        //         }
        //         else
        //         {
        //             //TODO:Application.Top.MenuBar.Menus.?
        //             ViewModel.StopProgressBar(_menuFileQuit.Help.ToString() + FormsViewModel<string, MVCSettings, MVCModel, MvcView>.ACTION_IN_PROGRESS + " cancelled");
        //         }

        //     }
        //     catch (Exception ex)
        //     {
        //         if (ViewModel != null)
        //         {
        //             ViewModel.ErrorMessage = ex.Message.ToString();
        //             ViewModel.StatusMessage = "";
        //         }

        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }
        #endregion Actions

        #region Utility
        /// <summary>
        /// Bind static Model controls to Model Controller
        /// </summary>
        // private static void BindFormUi()
        // {
        //     try
        //     {
        //         //Form

        //         //Controls
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// Bind Model controls to Model Controller
        // /// Note: databinding not available in Terminal.Gui, but leave this in place
        // ///  in case I figure out a way to do this
        // /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
		// 		BindField(_txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
		// 		BindField(_txtSomeString, ModelController<MVCModel>.Model, "SomeString");
		// 		BindField(_chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");
		// 		BindField(_dtSomeDate, ModelController<MVCModel>.Model, "SomeDate", "Text");

		// 		BindField(_txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
		// 		BindField(_txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
		// 		BindField(_chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");
		// 		BindField(_dtSomeOtherDate, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherDate", "Text");

		// 		BindField(_txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
		// 		BindField(_txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
		// 		BindField(_chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
		// 		BindField(_dtStillAnotherDate, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherDate", "Text");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// Note: databinding not available in GtkSharp, but leave this in place
        // ///  in case I figure out a way to do this
        // private void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     where TControl : View
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO: ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic
        // ///  to be triggered by refresh?
        // /// </summary>
        // /// <param name="functionButton">Button</param>
        // /// <param name="functionButton">Button</param>
        // /// <param name="functionMenu">MenuItem</param>
        // /// <param name="cancelButton">Button</param>
        // /// <param name="enable">bool</param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     Button functionToolbarButton,
        //     MenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.Enabled = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.Enabled = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             //TODO:functionMenu.Enabled = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.Enabled = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Invoke any delegate that has been registered
        // ///  to cancel a long-running background process.
        // /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;
			try
			{
               // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

				returnValue = true;
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				//throw;
			}
			return returnValue;
        }

        private void BindSizeAndLocation()
        {
           //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));

            //Note:these do not play well with Terminal.Gui, and the right/bottom frame disappears
            // Height = Properties.Settings.Default.Size.Height;
            // Width = Properties.Settings.Default.Size.Width;

            //TODO:this.Position.X =  global::MvcForms.Core.AvaloniaUI.Properties.Settings.Default.Location.X;
            //TODO:this.Position.Y =  global::MvcForms.Core.AvaloniaUI.Properties.Settings.Default.Location.Y;
        }
        //TODO:integrate these into ViewModel?
        private string GetStatusMessage()
        {
            return _statusMessage.Title.ToString();
            // return Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Status].Title.ToString();
        }

        private void SetStatusMessage(string statusMessage)
        {
            _statusMessage.Title = statusMessage;
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Status].Title = statusMessage;
            SetNeedsDisplay();
        }

        private void SetErrorMessage(string errorMessage)
        {
            _errorMessage.Title = errorMessage;
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Error].Title = errorMessage;
            SetNeedsDisplay();
        }

        private void StartProgressBar()
        {
            // _progressBar.Fraction = 0.33;
            // _progressBar.PulseStep=0.1;
            // _progressBar.Visible = true;
            // _progressBar.Pulse();
            _progressBar.Title = StatusBar_MarqueeOn;
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Progress].Title = StatusBar_MarqueeOn;
            SetNeedsDisplay();
        }

        private void StopProgressBar()
        {
            // _progressBar.Pulse();
            //Application.DoEvents();
            // _progressBar.Visible = false;
            // _progressBar.Fraction = 0.0;
            // _progressBar.PulseStep=0.0;
            _progressBar.Title = StatusBar_MarqueeOff;
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Progress].Title = StatusBar_MarqueeOff;
            SetNeedsDisplay();
        }

        private void StartActionIcon(string actionName)
        {
            _pictureAction.Title = actionName;
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Action].Title = actionName;
            SetNeedsDisplay();
        }

        private void StopActionIcon()
        {
            _pictureAction.Title = "";
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Action].Title = "";
            SetNeedsDisplay();
        }

        private void SetDirtyIcon(bool isSet)
        {
            _pictureDirty.Title = isSet ? "Dirty" : "";
            // Application.Top.StatusBar.Items[(int)StatusBar_ItemIndex.Dirty].Title = (isSet ? "Dirty" : "");
            SetNeedsDisplay();
        }

        //TODO:adapt this sample  to my statusbar
        // var progress = new ProgressBar (new Rect (68, 1, 10, 1));
        // bool timer (MainLoop caller)
        // {//TODO:use DoEvents, to make more responsive than original sample?
        // 	progress.Pulse ();
        // 	return true;
        // }
        // statusBarFrameView.Add (new Label ("label in frameview") {
        //     X = 0,Y = 0
        // });
        // Application.MainLoop.AddTimeout (TimeSpan.FromMilliseconds (300), timer);
        #endregion Utility
        #endregion Methods
    }
}