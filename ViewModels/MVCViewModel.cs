﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.GuiCs;
using MvcLibrary.Core;
using Terminal.Gui;

namespace MvcForms.Core.GuiCs
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel<string, MVCSettings, MVCModel, MvcView>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, string> actionIconImages,
            FileDialogInfo<Window, string> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            string errorMessage = null;
            FileDialogInfo<Window, string> fileDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo<Window, string>
                (
                    parent: null,
                    modal: true,
                    title: "Select Folder...",
                    response: Button_None
                )
                {
                    SelectFolders = true,
                    InitialDirectory = Environment.SpecialFolder.Personal
                };

                if (!Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != Button_None)
                {
                    if (fileDialogInfo.Response == Button_OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    return; //if dialog was cancelled
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Preferences failed: {0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Window, string, string> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Font...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );
                fontDialogInfo = new FontDialogInfo<Window, string, string>
                (
                    parent: null,
                    modal: true,
                    title: "Select Font",
                    response: Button_None,
                    fontDescription: default
                );

                if (Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))//Note:will throw not implemented exception
                {
                    if (fontDialogInfo.Response == Button_OK)
                    {
                        StatusMessage += fontDialogInfo.FontDescription;
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Window, string, string> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Color...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );
                colorDialogInfo = new ColorDialogInfo<Window, string, string>
                (
                    parent: null,
                    modal: true,
                    title: "Select Color",
                    response: Button_None,
                    color: default
                );

                if (Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {//TODO:will throw not implemented exception
                    if (colorDialogInfo.Response == Button_OK)
                    {
                        StatusMessage +=
                            string.Format
                            (
                                "Red/Green/Blue:{0}",
								colorDialogInfo.Color
							);
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }
        #endregion Methods

    }
}
